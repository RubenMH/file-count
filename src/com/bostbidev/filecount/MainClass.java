package com.bostbidev.filecount;

import java.io.File;

public class MainClass {
	
	private static int mCount = 0;
	
	public static void main(String[] args) {
		
		System.out.println();
		System.out.println(" #######                          #####                                 ");
		System.out.println(" #        #  #       ######      #     #   ####   #    #  #    #  ##### ");
		System.out.println(" #        #  #       #           #        #    #  #    #  ##   #    #   ");
		System.out.println(" #####    #  #       #####       #        #    #  #    #  # #  #    #   ");
		System.out.println(" #        #  #       #           #        #    #  #    #  #  # #    #   ");
		System.out.println(" #        #  #       #           #     #  #    #  #    #  #   ##    #   ");
		System.out.println(" #        #  ######  ######       #####    ####    ####   #    #    #   ");
		System.out.println("                                                            by Bostbidev");
		System.out.println();
		System.out.println();
		System.out.println();
		
		if ( args.length > 0 ) {
			System.out.println("wait...");
			System.out.println();
			System.out.println();
			System.out.println();
			runDown(new File(args[0]));
			System.out.println("Files: " + mCount);
			System.out.println("");
			System.out.println("");
		}
		else {
			System.out.println("You need to pass the path");
			System.out.println("");
			System.out.println("");
		}
	}
	
	
	public static void runDown(File dir) {
		File listFile[] = dir.listFiles();
		for (int i = 0; i < listFile.length; i++) {
			if (listFile[i].isDirectory()) {
				runDown(listFile[i]);
			} else {
				mCount++;
			}
		}
	}

}
